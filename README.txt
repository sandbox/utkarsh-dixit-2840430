
INTRODUCTION
------------

Social Image Share allows to share images on a node. This module
allows the user to share images on facebook, google plus,
pininterest and twitter.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/utkarsh-dixit/2840430

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2840430

REQUIREMENTS
------------

No special requirements 

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://goo.gl/pj7L2w
   for further information.

CONFIGURATION
-------------

 * Customize the Social Image Share settings in Administration » Configuration »
   Social Image Share Settings.

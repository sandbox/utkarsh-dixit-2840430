<?php

namespace Drupal\social_image_share\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 *
 * @package Drupal\social_image_share\Form
 */
class SocialImageShareForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'social_image_share.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_image_share.settings');
    $form['custom_elements'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Custom Elements'),
      '#default_value' => $config->get('custom_elements'),
      '#placeholder' => 'Enter the list of elements as css selectors separated by a comma. Example:- img, video',
    );
    $form['buttons_styles'] = array(
      '#type' => 'radios',
      '#title' => $this->t('Buttons Style'),
      '#options' => array(
        0 => $this->t('Rounded Buttons'),
        1 => $this->t('Flat Buttons'),
      ),
      '#default_value' => $config->get('buttons_styles'),
    );
    $form['social_buttons'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Social Buttons',
      '#options' => array(
        1 => $this->t("Facebook"),
        2 => $this->t("Twitter"),
        3 => $this->t("Google Plus"),
        4 => $this->t("Pinterest"),
      ),
      '#default_value' => $config->get('social_buttons', array()),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config_form = $this->config('social_image_share.settings');
    $config_form->set('custom_elements', $form_state->getValue('custom_elements'))->save();
    $config_form->set('buttons_styles', $form_state->getValue('buttons_styles'))->save();
    $config_form->set('social_buttons', $form_state->getValue('social_buttons'))->save();
  }

}
